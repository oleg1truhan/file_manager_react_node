export const getData = (setData, setParent) => {
    fetch('http://localhost:8000/api')
        .then(res => res.json())
        .then(result => {
                setParent('');
                setData(result);
            },
            (error) => {

            }
        )
}

export const getDataForClick = (setData, setParent, event) => {
    fetch('http://localhost:8000/api/?path=' + event.target.attributes.href.value)
        .then(res => res.json())
        .then(result => {
                let linkArr = result.path.split('/');
                linkArr.pop();
                setParent(linkArr.join('/'));
                setData(result);
            },
            (error) => {

            }
        )
}
