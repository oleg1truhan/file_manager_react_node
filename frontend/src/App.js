import React, {useEffect, useState} from 'react';
import {getData, getDataForClick} from './service/service';
import './App.css';

function App() {
    const [parent, setParent] = useState('');
    const [data, setData] = useState({
        path: '',
        files: [],
    });

    useEffect(() => {
        getData(setData, setParent);
    }, []);

    const clickHandler = (event) => {
        event.preventDefault();
        getDataForClick(setData, setParent, event);
    }

    return (
        <div className="file-manager">

            <div>
                <a href={parent} onClick={clickHandler}>
                    <span className='material-icons'>&#xe5d8;</span>
                    LEVEL UP
                </a>
            </div>

            <div className="current-level">
                current: {data.path === '' ? '/' : data.path}
            </div>

            <ul className='folder-list'>
                {data.files.map(item => {

                    if (item.dir) {
                        return <li key={item.name} className='folder'>
                            <a href={data.path + '/' + item.name} onClick={clickHandler}>
                                <span className='material-icons'>&#xe2c7;</span>
                                {item.name.toUpperCase()}
                            </a>
                        </li>
                    } else {
                        return <li key={item.name} className='file'>
                            <span className='material-icons'>&#xe873;</span>
                            {item.name}
                        </li>
                    }

                })}
            </ul>
        </div>
    );
}

export default App;
