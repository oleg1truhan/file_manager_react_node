import fs from 'fs';

export const isFolder = (path) => {
    return fs.lstatSync(path).isDirectory() && fs.existsSync(path);
}
