import {Router} from 'express';
import {mainController} from '../controller/main.controller.js';

export const mainRouter = new Router();

mainRouter.get('/', mainController);
