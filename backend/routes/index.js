import {Router} from 'express';
import {mainRouter} from "./main.router.js";

export const router = new Router();

router.use('/', mainRouter);
